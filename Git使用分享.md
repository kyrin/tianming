![git](https://s14.postimg.org/hy1azlxpt/WX20170814-134003_2x.png)
### ❖  SSH模式访问GitLab
> 参考:[http://jira.linkedsee.com/browse/PREP-3?jql=text%20~%20%22git%22 ](http://jira.linkedsee.com/browse/PREP-3?jql=text%20~%20%22git%22 )


设置步骤:  

+ 本地进入`.ssh`查看是否存在密钥对:xxx和xxx.pub  
    命令:`cd ~/.ssh`
    
+ 如果不存在，使用ssh-keygen来创建  
    命令:`ssh-keygen -t rsa -C "youremail@youremail.com"`  
    例如:`ssh-keygen -t rsa -C "xiaoming@yun-ji.cn"`  
    注解:  
    `Enter file in which to save the key` 输入保存秘钥的文件 直接enter即可
    `Enter passphrase (empty for no passphrase)` 输入密码 直接enter即可
    此时查看`.ssh`目录下可看到新增的一对秘钥`id_rsa`和`id_rsa.pub`
    
+ 查看公钥  
    命令: `cat ~/.ssh/id_rsa.pub `  
    复制全部，包括后面的邮箱
    
+ 添加到GitLab中  
    左侧栏Profile Settings → 左侧栏SSH Keys → 粘贴并Add key
    
+ 创建config，并更改权限
    命令:`cat>~/.ssh/config`  
    输入: 
    
    ``` 
    Host gitlab.linkedsee.com   
    User git   
    Port 458
    IdentityFile /home/yourname/.ssh/id_rsa（替换成你的id_rsa所在的路径）
    ```
    更改权限:  
    `chmod 600 /home/yourname/.ssh/config`

+ 更改remote  
    使用`git remote -v`查看origin和upstream的url，把http更改为ssh地址  
    命令:`git remote set-url origin（或者upstream） xxxx`  
    例如:
    
    ```
    git remote add origin git@gitlab.linkedsee.com:xiaoming/server.git
    git remote set-url origin git@gitlab.linkedsee.com:xiaoming/server.git
    git remote add upstream git@gitlab.linkedsee.com:enterprise/server.git
    git remote set-url upstream git@gitlab.linkedsee.com:enterprise/server.git
    ```
    如果set-url 失败，则先执行add
    <center>![](https://s14.postimg.org/lq3oduso1/QQ20180226-155253_2x.png)</center>
+ 验证是否设置成功  
    命令:`ssh -T git@gitlab.linkedsee.com`  
    显示`Welcome to GitLab, yourname!` 代表成功。
    <center>![](http://chuantu.biz/t6/237/1519631443x-1404793585.png)</center>
    
### ❖  工作区，暂存区，本地仓库，远程仓库
> Git 是分工作区的：
正在执行的叫working directory区，想暂存的叫staging area，最终 commit 的叫 git directory 区。

	文档有4种状态
	Untracked files:正在working directory区的有未被追踪的文档，一般是新建的文档，界面中用红色显示
	Changes not staged for commit:已修改但还未放入暂存的文件，一般是旧文档进行修改的，界面中用红色显示
	changes to be committed:已经放入暂存的文件，可以直接 commit，显示绿色
	Commited:已经被 commit 到 git 中。
<center>![工作分区](https://s14.postimg.org/58d1zhq41/QQ20180227-181648.png)</center>
    
### ❖  项目代码管理 
+ 获取git项目仓库
  - `git clone git://git.kernel.org/pub/scm/.../linux.git`
  - clone 默认会把后面的url设为origin的追踪
  - 每一个本地库下都有一个.git的隐藏文件夹，文件夹中的文件保存着跟这个本地库相关的信息


+ 更新本地库
  - `git fetch --origin/upstream/all`
     - 只会将本地库所关联的远程库的commit id更新至最新
  - `git pull <branch name>`:
     - 更新本地库≈fetch + merge


+ 合并分支
  - `git merge <branch name>`
  - `git pull -r <branch name>`
     - log会按照提交历史正确排序，且不会产生merge的log
     - 如果有冲突，可以用--abort/add/--continue 放弃rebase或者解决冲突
     
     
+ 查看提交历史
  - 查看最近的几条commit:`git log -n`
  - 查看单个文件的更新历史：`git log --follow filename`
  - 查看commit的更新内容:`git log -p`![](https://s14.postimg.org/hzdg14yf5/QQ20180227-132309.png)

  
+ 切换分支
  - 切换指定分支:`git checkout <branch name>`
  - 创建新的分支:`git checkout -b <branch name>`
  - 切到指定的commit:`git checkout <commit id>`
  - 从指定commit切出新分支:`git checkout -b <branch name> <commit id>`

  
+ branch
  - 查看所有分支:`git branch -av`![](http://chuantu.biz/t6/238/1519708725x-1404781210.png)
  - 查看分支追踪信息:`git branch -vv`![](https://s14.postimg.org/6zs8pgv4h/QQ20180227-131945.png)
  - 删除分支:`git branch -D <branch name>`
  - 解除分支的追踪:`git branch --unset-upstream`


+ 查看文件更新内容
  - 查看更新的文件:`git status`
  - 查看更新内容:`git diff / git diff <filename>`
  - 查看已经暂存的更新文件:`git diff --cached` / `git diff --staged`
  
  
+ 提交修改
  - 将文件暂存:`git add . / git add <filename>`
  - 提交暂存区:`git commit -m <commit message>`
  - 修改最后一次提交的commit 信息:`git comit --amend`


+ 恢复文件改动
  - `git checkout <filename>`
  
  
+ 推送代码
  - `git push origin <branch name>`
 
+ reset
  - `git reset  --hard`:彻底回退到某个版本，本地的源码也会变为上一个版本的内容
  - `git reset --soft`:回退到某个版本，只回退了commit的信息，不会恢复到index file一级。如果还要提交，直接commit即可
  - `git reset --mixed`:此为默认方式，不带任何参数的git reset，即时这种方式，它回退到某个版本，只保留源码，回退commit和index信息

+ 删除git追踪的文件:`git rm <filename>`


+ stash:可用来暂存当前正在进行的改动
  - 暂存改动:`git stash`
  - 查看所有stash:`git stash list`
  - 产看某一次stash的内容:`git stash show <stash name>` (-u   显示改动详情 )
  - 恢复stash的改动:`git stash pop` / `git stash pop <stash name>` / `git stash apply <stash name>`
  - 删除stash: `git stash drop <stash name>` / `git stash clear`

+ 显示某个commit或者tag的内容:`git show <commit id>` / `git show <tag name>`
+ 清理远程不存在的本地分支
  - `git fetch -p / --prune` 

### ❖  提交代码规范
+ add: pass
+ commit
  - meaasge:[JIRA编号:]  [完成的功能]<center>![](https://s14.postimg.org/5d7mhtvk1/QQ20180227-142129_2x.png)</center>

+ merge request:<center>![](https://s14.postimg.org/pyme9nd41/QQ20180227-142823_2x.png)</center>
+ rebase
  - 合并提交:`git rebase -i HEAD~<n>`   将最近的n个commit合并为一个
+ cherry-pick
  - 应用commit，大多用于不同分支之间:`git cherry-pick <commit hash>`


### ❖  代码提交流程
<center>![代码提交流程](https://s14.postimg.org/5ikkjb2ap/QQ20180227-180600.png)</center>

### ❖  版本发布管理
+ tag
  - 查看tag:`git tag / git tag -l`
  - 创建tag:`git tag -a <tag name> <commit id> -m <tag meaasge>`
  - 删除tag:`git tag -d <tag name>`
  - 推送tag:`git push origin <tag name>/--tags`
+ branch
  - 开发时可以创建不同的分支，分别完成单独的需求
  - 最终发布时，可根据实际需求创建不同的项目分支

### ❖  其他
+ Git 完整命令手册地址：[http://git-scm.com/docs](http://git-scm.com/docs) / `git <command> --help`
+ 自动补全<center>![](https://s14.postimg.org/bzyvol2kh/QQ20180227-170445.png)</center>
+ 别名:`~/.gitconfig | xx/.git/config`  <center>![](https://s14.postimg.org/j1wtaqvvl/QQ20180227-170011.png)</center>