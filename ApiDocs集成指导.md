## Flask 集成API Docs指导说明

> **依赖包**

+ [APIDOC](http://apidocjs.com/#demo)
+ [Flask_apidoc](https://github.com/viniciuschiele/flask-apidoc)


> **[效果图](http://apidocjs.com/example/)**

+ ![效果图](http://chuantu.biz/t6/125/1509786325x1780861130.png)


> **环境准备**

+ flask_apidoc
	+ pip install flask_apidoc
+ apidoc 
	+ 安装node.js  
		+ `curl —silent —location https://rpm.nodesource.com/setup_6.x | bash -
yum install -y nodejs`
	+ 检查node、npm版本
		+ `node -v`(v8.4.0), `npm -v` (5.3.0)---以此版本测试通过
	+ 安装apidoc
		+ `npm install apidoc -g`
	+ 检查apidoc
		+ `apidoc -h`![success](http://chuantu.biz/t6/124/1509783693x1780861130.png)

> **项目结构**

+ 初始化ApiDoc  

```python
# tristram/tristram/libs/docs.py
# -*- coding: utf8 -*-
from flask_apidoc import ApiDoc


def api_docs(app):
    ApiDoc(app=app, url_path='/api_docs')
```
+ Init app

```python
# tristram/tristram/app.py
# -*- coding: utf8 -*-
import os
import sys
from flask import Flask
from tristram.config import config
from tristram.libs.docs import api_docs

reload(sys)
sys.setdefaultencoding('utf-8')


def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('TRISTRAM_CONFIG', 'development')
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    # Init API Docs
    api_docs(app=app)

    return app


app = create_app()
```

+ 添加ApiDoc启动配置文件

```json
# tristram/apidoc.json(项目根目录)
{
  "name": "TRISTRAM API DOCS",
  "version": "1.0.0",
  "description": "Tristram Server API example",
  "title": "Innet Server API example",
  "url" : "http://127.0.0.1:9000", # 端口与server相同
  "sampleUrl": "http://127.0.0.1:9000",
  "template": {
    "withCompare": true,
    "withGenerator": true,
    "forceLanguage": "zh_cn"
  }
}
```

+ manager调试入口增加渲染模版方法
	+ 建议将官方默认模版添加到项目中，[地址](https://github.com/apidoc/apidoc/tree/master/template)

```python
# tristram/manage.py
#!/usr/bin/env python
# -*- coding: utf8 -*-
import sys
import fire
from tristram.app import app
from flask_apidoc.commands import GenerateApiDoc

reload(sys)
sys.setdefaultencoding('utf-8')


class Manager(object):

    """
    Manager: Net tristram tool. option '--help'

    usage: ./manage.py runserver 127.0.0.1:5000
           ./manage.py init_db
    """
    @classmethod
    def runserver(cls, host='127.0.0.1:5000'):
        """run server 127.0.0.1:5000"""
        host, port = host.split(':')
        print app.url_map
        cls.api_doc()
        app.run(host=host, port=int(port))

    @classmethod
    def api_doc(cls):
    	  # template_path 模版路径  output_path 渲染出的静态文件路径
        GenerateApiDoc(output_path="./tristram/static/docs",
                       template_path="./tristram/doc_template").run()
        print 'Docs is running ......'


if __name__ == '__main__':
    fire.Fire(Manager)
```

+ 检查doc路由是否成功添加
	+ runserver的部分加入`print app.url_map`,正常应该打印出doc的路由![](http://chuantu.biz/t6/125/1509787613x1780861130.png)

+ 启动项目，检查doc是否成功
	+ python manage.py runserver 0.0.0.0：9000
	+ 访问127.0.0.1：9000/api_docs
	+ 页面能正常访问

+ api语法参见：[APIDOC](http://apidocjs.com/#demo)