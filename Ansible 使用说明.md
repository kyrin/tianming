## Ansible 使用说明
[使用手册+配置详解](http://www.178linux.com/57409)
#### 1. 安装ansible环境
```yum install ansible```  
```yum install libselinux-python```
#### 2. 目标主机配置

+ 目标主机应与ansible的server端建立ssh通信

#### 3. 配置文件修改

+ ```vim /etc/ansible/hosts```  
![](http://chuantu.biz/t6/3/1502698570x1780861130.png)
+ ```vim /etc/ansible/hosts```（避免ansible执行过程中需要多次确认ssh通信请求）![](http://chuantu.biz/t6/3/1502698601x1780861130.png)

#### 4. ansible执行配置yml(deploy_pinger.yml)
```
---
- hosts: pinger
  serial: 1
  vars:
     #local_ip: "{{ ansible_eth1['ipv4']['address'] }}"
     local_ip: "{{ ip }}"
     agent_files: "{{ pinger_files }}"
     server_ip: "{{ server_ip }}"
  remote_user: root
  tasks:
   - name: copy time files
     copy: src=/root/ansible_test/localtime dest=/etc

   - name: copy pinger files
     copy: src=/root/ansible_test/pinger.tar.gz dest=/root

   - name: copy python files
     copy: src=/root/ansible_test/site-packages.tar.gz dest=/root

   - name: execute deploy pinger scripts
     script: /root/ansible_test/handler.sh "{{ agent_files }}" "{{ server_ip }}" "{{local_ip}}"
     ignore_errors: yes
     register: output

   - name: copy crontab files
     copy: src=/root/ansible_test/root dest=/var/spool/cron

   - name: execute update crontab scripts
     script: /root/ansible_test/update_crontab.sh
     ignore_errors: yes
     register: output

   - debug: msg='{{ output.stdout_lines }}'
   - debug: msg='{{ output.stderr }}'
```

#### 5. sh执行脚本
+ handler.sh

```
#!/bin/bash


function Color_Text()
{
  echo -e " \e[0;$2m$1\e[0m"
}

function Echo_Yellow()
{
  echo $(Color_Text "$1" "33")
}

function install_dependence(){
    cur_dir=$(pwd)

    echo "安装系统依赖 . . ."
    #yum -y install epel-release;
    #yum -y install gcc python-pip;

    #pip install -r ${cur_dir}/pip-req.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
    echo "python 依赖安装完成 . . ."

    cp ${cur_dir}/pinger.py /usr/local/bin/
    echo  "*/1 * * * * python /usr/local/bin/pinger.py  >> /root/pinger.log"  >> /var/spool/cron/root
    echo "crontab 任务已成功添加 . . ."
}

SERVERIP=$2
LOCALIP=$3
echo "i'm running"
echo $1
echo $2
echo $3
tar -zxvf /root/pinger.tar.gz
tar -zxvf /root/site-packages.tar.gz -C /lib/python2.7
cd /root/pinger
sed -i  "s/ServerIP/${SERVERIP}/g" /root/pinger/pinger.py
sed -i  "s/LocalIP/${LOCALIP}/g" /root/pinger/pinger.py

install_dependence


```

+ update_crontab.sh

```
#!/bin/bash

chmod 600 /var/spool/cron/root
```

#### 5. 执行ansible脚本
`ansible-playbook deploy_pinger.yml -v --extra-vars "pinger_files=/root/ansible_test/pinger.tar.gz  server_ip=103.37.161.80"`